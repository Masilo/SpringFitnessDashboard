package repository;

import model.User;

import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
public interface UserRepository {
    List<User> findAll();
}
