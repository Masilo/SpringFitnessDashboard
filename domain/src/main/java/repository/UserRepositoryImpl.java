package repository;

import model.User;
import org.springframework.stereotype.Repository;
import services.FitnessService;
import services.FitnessServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {

    //Some sort of data retrieval from DB

    @Override
    public List<User> findAll(){
        List<User> users = new ArrayList<>();

        User user1 =  new User("Bokang1","Masilo",20);
        User user2 =  new User("Bokang2","Masilo",24);
        User user3 =  new User("Bokang3","Masilo",26);

        user1.setWeight(57.3);
        user2.setWeight(67.3);
        user3.setWeight(77.3);

        user1.setHeight(1.35);
        user2.setHeight(1.45);
        user3.setHeight(1.55);

        FitnessService fitnessService1 = new FitnessServiceImpl(user1.getWeight(),user1.getHeight());
        FitnessService fitnessService2 = new FitnessServiceImpl(user2.getWeight(),user2.getHeight());
        FitnessService fitnessService3 = new FitnessServiceImpl(user3.getWeight(),user3.getHeight());

        user1.setFitnessService(fitnessService1);
        user2.setFitnessService(fitnessService2);
        user3.setFitnessService(fitnessService3);
        users.add(user1);
        users.add(user2);
        users.add(user3);

        return users;
    }
}
