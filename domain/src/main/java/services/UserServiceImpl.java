package services;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import repository.UserRepository;

import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */

@Component("userService")
public class UserServiceImpl implements UserService {
    //private UserRepository userRepository = new UserRepositoryImpl(); Hard coded
    // The UserServiceImpl shouldn't know that its using UserRepositoryImpl Specifically

    @Autowired
    //config for auto Injection
    private UserRepository userRepository;

    //@Autowired
    //config for constructor Injection
    public UserServiceImpl(UserRepository userRepository){
        System.out.println("Using Constructor Injection");
        this.userRepository = userRepository;
    }

    //config for setter Injection
    public UserServiceImpl(){

    }

    //@Autowired
    //config for setter Injection
    public void setUserRepository(UserRepository userRepository){
        System.out.println("Using Setter Injection");
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll(){
        return userRepository.findAll();
    }
}
