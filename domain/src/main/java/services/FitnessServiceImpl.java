package services;

import java.text.DecimalFormat;

/**
 * Created by abednigo.masilo on 2017/06/21.
 */
public class FitnessServiceImpl implements FitnessService {
    private double weight;
    private double height;

    @Override
    public double getBmi(double height, double weight){
        this.weight = weight;
        this.height = height;
        return getBmi();
    }

    public FitnessServiceImpl(double weight, double height){
        this.weight = weight;
        this.height = height;
    }

    @Override
    public double getBmi(){
        double bmiLong = (weight/(height*height));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        double bmi = Double.valueOf(decimalFormat.format(bmiLong));
        return bmi;
    }
}
