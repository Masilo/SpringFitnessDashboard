package services;

import model.User;

import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
public interface UserService {
    List<User> findAll();
}
