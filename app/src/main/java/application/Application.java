package application;

import model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import services.UserService;

import java.util.Scanner;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
public class Application {
    public static void main(String args[]){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        UserService service = applicationContext.getBean("userService", UserService.class);

        // for all users (Admin Dashboard)
        for(User user: service.findAll()){
            System.out.println(user.getFirstname()+" has a BMI of "+user.getFitnessService().getBmi());
        }

        // for a particular user (User 0 Dashboard)
        int id = 0;
        User user = service.findAll().get(id);
        String userName = user.getFirstname();
        double userBmi = user.getFitnessService().getBmi();
        System.out.println("\n"+userName+" has a BMI of "+userBmi);

        // Console (UI) capturing of data
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter body weight: ");
        double weight = scanner.nextDouble();

        System.out.println("Please enter body height: ");
        double height = scanner.nextDouble();

        userBmi = user.getFitnessService().getBmi(height,weight);
        double userBmi2 = user.getFitnessService().getBmi();

        System.out.println("\n"+user.getFirstname()+" has a new BMI of "+userBmi + "+"+ userBmi2);
    }
}
