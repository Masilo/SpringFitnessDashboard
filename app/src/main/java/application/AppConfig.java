package application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
@Configuration
@ComponentScan({"repository","services"}) // Scan the repository package (we have "customerRepository" bean in there... similar with services
public class AppConfig {

/*    @Bean(name = "userService")
    public UserService getCustomerService(){
        // Constructor Injection
        UserServiceImpl service = new UserServiceImpl();
        return service;
    }*/

   /* @Bean(name = "userRepository")
    public UserRepository getUserRepository(){
        return  new UserRepositoryImpl();
    }*/
}
